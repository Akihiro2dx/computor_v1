mod lib;
use lib::equation::Equation;
use lib::equation::Hand;

fn main() {
    let eq = Equation {
        left_hand: Hand(vec![2.0, 1.0, 10.0, 4.5]),
        right_hand: Hand(vec![0.0, 1.0, 0.0,]),
    };

    println!("The equation is {}", eq);
    println!("The reduced form of the equation is {}", eq.reduce());
    println!("The polynomial degree of the equation is {}", eq.degree());
    eq.solve();
}