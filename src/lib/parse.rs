use lib::equation::Equation;
use lib::equation::Hand;

pub fn parse(expr: &str) -> Equation {
    Equation {
        left_hand: Hand(vec![0.0]),
        right_hand: Hand(vec![0.0]),
    }
}

#[cfg(test)]
mod test {

    use lib::parse::parse;
    use lib::equation::Equation;
    use lib::equation::Hand;

    #[test]
    fn parse_zero() {
        let expect = Equation {
            left_hand: Hand(vec![0.0]),
            right_hand: Hand(vec![0.0]),
        };

        assert_eq!(parse("0"), expect);
    }
}
