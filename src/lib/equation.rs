use lib::complex::Complex;
use std::cmp;
use std::fmt;

#[derive(Debug, PartialEq)]
pub struct Equation {
    pub left_hand: Hand,
    pub right_hand: Hand,
}

#[derive(Debug, PartialEq)]
pub struct Hand(pub Vec<f32>);

impl fmt::Display for Hand {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let mut iter = self.0.iter().enumerate().filter(|x| *x.1 != 0.0);

        match iter.next() {
            Some(el) => {
                write!(fmt, "{} * X^{}", el.1, el.0);
            }
            None => {
                write!(fmt, "0");
            }
        };

        for t in iter {
            write!(fmt, " + {} * X^{}", t.1, t.0);
        }
        Ok(())
    }
}

impl fmt::Display for Equation {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{} = {}\n", self.left_hand, self.right_hand)
    }
}

impl Equation {
    pub fn reduce(&self) -> Equation {
        let mut reduced = Equation {
            left_hand: Hand(self.left_hand.0.clone()),
            right_hand: Hand(vec![0.0]),
        };

        while reduced.left_hand.0.len() < self.right_hand.0.len() {
            reduced.left_hand.0.push(0.0);
        }

        for i in 0..self.right_hand.0.len() {
            reduced
                .left_hand.0[i] = self.left_hand.0[i] - self.right_hand.0[i];
        }
        reduced
    }

    pub fn degree(&self) -> usize {
        let max_degree_in_hand = |hand: &Hand| {
            let mut degree = 0;
            for (deg, val) in hand.0.iter().enumerate() {
                if *val != 0.0 && deg > degree {
                    degree = deg;
                }
            }
            degree
        };

        cmp::max(
            max_degree_in_hand(&self.left_hand),
            max_degree_in_hand(&self.right_hand),
        )
    }

    pub fn discriminant(&self) -> Result<f32, &str> {
        match self.degree() {
            2 => Ok(self.left_hand.0[1] * self.left_hand.0[1]
                - 4.0 * self.left_hand.0[0] * self.left_hand.0[2]),
            _ => Err("Equation is not of degree 2"),
        }
    }

    pub fn solve(&self) {
        match self.degree() {
            1 => self.solve_first_degree(),
            2 => self.solve_second_degree(),
            _ => println!("No solver for this degree of equation !"),
        }
    }

    fn solve_first_degree(&self) {
        let reduced = self.reduce();
        let a = reduced.left_hand.0[1];
        let b = reduced.left_hand.0[0];

        if a == 0.0 && b != 0.0 {
            println!("No solutions !");
        } else if a == 0.0 && b == 0.0 {
            println!("∀ x ∊ ℝ, f(x) = 0",)
        } else {
            println!("One real solution: {}", -b / a);
        }
    }

    fn solve_second_degree(&self) {
        let reduced = self.reduce();
        let a = reduced.left_hand.0[2];
        let b = reduced.left_hand.0[1];
        let delta = reduced.discriminant().unwrap();

        match cmp::PartialOrd::partial_cmp(&self.discriminant().unwrap(), &0.0) {
            Some(std::cmp::Ordering::Less) => println!(
                "Two complex solutions : {} and {}",
                Complex(-b, delta.abs().sqrt()) / Complex(2.0 * a, 0.0),
                Complex(-b, -delta.abs().sqrt()) / Complex(2.0 * a, 0.0)
            ),
            Some(std::cmp::Ordering::Equal) => {
                println!("One real solution : {}", -b / 2.0 * a);
            }
            Some(std::cmp::Ordering::Greater) => {
                println!(
                    "Two real solutions : {0} and {1}",
                    -b + delta.sqrt() / 2.0 * a,
                    -b - delta.sqrt() / 2.0 * a,
                );
            }
            None => panic!("Wtf"),
        }
    }
}

#[cfg(test)]
mod test {

    use lib::equation::Equation;
    use lib::equation::Hand;

    #[test]
    fn should_print_correctly() {
        assert_eq!(Hand(vec![0.0]).to_string(), "0");
        assert_eq!(Hand(vec![1.0]).to_string(), "1 * X^0");
        assert_eq!(Hand(vec![1.0, 1.0]).to_string(), "1 * X^0 + 1 * X^1");
        assert_eq!(Hand(vec![-1.0, -1.0]).to_string(), "-1 * X^0 + -1 * X^1");
        assert_eq!(Hand(vec![2.0, 0.0, 3.0]).to_string(), "2 * X^0 + 3 * X^2");
        assert_eq!(Hand(vec![0.0, 2.0, 0.0]).to_string(), "2 * X^1");
    }

    #[test]
    fn should_reduce_correctly() {
        // 2x = 2
        let eq = Equation {
            left_hand: Hand(vec![2.0, 0.0]),
            right_hand: Hand(vec![0.0, 2.0])
        };

        // 2x - 2 = 0
        let reduced = Equation {
            left_hand: Hand(vec![2.0, -2.0]),
            right_hand: Hand(vec![0.0])
        };

        assert_eq!(eq.reduce(), reduced);
    }
}
