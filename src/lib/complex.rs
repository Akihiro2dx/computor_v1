// 0 is Real part, 1 is Im part
#[derive(Debug, PartialEq)]
pub struct Complex(pub f32, pub f32);

impl Complex {
    pub fn to_real(&self) -> Result<f32, &str> {
        if self.1 != 0.0 {
            Err("has an imaginary part !")
        } else {
            Ok(self.0)
        }
    }

    pub fn from_real(real: f32) -> Complex {
        Complex(real, 0.0)
    }
}

impl std::fmt::Display for Complex {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Complex(re, im) if *im == 0.0 => write!(fmt, "{}", re),
            Complex(re, im) if *re == 0.0 => {
                if *im == 1.0 {
                    write!(fmt, "i")
                } else if *im == -1.0 {
                    write!(fmt, "-i")
                } else {
                    write!(fmt, "{}i", im)
                }
            }
            Complex(re, im) if (*im).abs() == 1.0 => {
                write!(fmt, "{}", re);
                if *im < 0.0 {
                    write!(fmt, " - i")
                } else {
                    write!(fmt, " + i")
                }
            }
            Complex(re, im) => {
                write!(fmt, "{}", re);
                if *im < 0.0 {
                    write!(fmt, " - {}i", -im)
                } else {
                    write!(fmt, " + {}i", im)
                }
            }
        }
    }
}

impl std::ops::Add for Complex {
    type Output = Complex;

    fn add(self, other: Complex) -> Complex {
        Complex(self.0 + other.0, self.1 + other.1)
    }
}

impl std::ops::Sub for Complex {
    type Output = Complex;

    fn sub(self, other: Complex) -> Complex {
        Complex(self.0 - other.0, self.1 - other.1)
    }
}

impl std::ops::Mul for Complex {
    type Output = Complex;

    fn mul(self, other: Complex) -> Complex {
        Complex(
            self.0 * other.0 - self.1 * other.1,
            self.0 * other.1 + self.1 * other.0,
        )
    }
}

impl std::ops::Div for Complex {
    type Output = Complex;

    fn div(self, other: Complex) -> Complex {
        Complex(
            (self.0 * other.0 + self.1 * other.1) / (other.0.powf(2.0) + other.1.powf(2.0)),
            (self.1 * other.0 - self.0 * other.1) / (other.0.powf(2.0) + other.1.powf(2.0)),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_display_correctly() {
        assert_eq!(Complex(0.0, 0.0).to_string(), "0");
        assert_eq!(Complex(1.0, 0.0).to_string(), "1");
        assert_eq!(Complex(0.0, 1.0).to_string(), "i");
        assert_eq!(Complex(2.0, 1.0).to_string(), "2 + i");
        assert_eq!(Complex(0.0, 2.0).to_string(), "2i");
        assert_eq!(Complex(3.0, 2.0).to_string(), "3 + 2i");
        assert_eq!(Complex(-1.0, 0.0).to_string(), "-1");
        assert_eq!(Complex(-1.0, 1.0).to_string(), "-1 + i");
        assert_eq!(Complex(-2.0, 1.0).to_string(), "-2 + i");
        assert_eq!(Complex(-1.0, 2.0).to_string(), "-1 + 2i");
        assert_eq!(Complex(0.0, -1.0).to_string(), "-i");
        assert_eq!(Complex(0.0, -2.0).to_string(), "-2i");
        assert_eq!(Complex(3.0, -1.0).to_string(), "3 - i");
        assert_eq!(Complex(-3.0, -1.0).to_string(), "-3 - i");
        assert_eq!(Complex(-3.0, -2.0).to_string(), "-3 - 2i");
    }

    #[test]
    fn should_convert_properly() {
        assert_eq!(Complex::from_real(3.0), Complex(3.0, 0.0));
        assert_eq!(Complex(3.0, 0.0).to_real().unwrap(), 3.0);
    }

    #[should_panic]
    #[test]
    fn should_panic_when_convert_im_to_real() {
        Complex(3.0, 1.0).to_real().unwrap();
    }

    #[test]
    fn should_add_correctly() {
        assert_eq!(Complex(1.0, 0.0) + Complex(1.0, 0.0), Complex(2.0, 0.0));
        assert_eq!(Complex(0.0, 1.0) + Complex(0.0, 1.0), Complex(0.0, 2.0));
        assert_eq!(Complex(1.0, 1.0) + Complex(1.0, 1.0), Complex(2.0, 2.0));
    }

    #[test]
    fn should_sub_correctly() {
        assert_eq!(Complex(2.0, 0.0) - Complex(1.0, 0.0), Complex(1.0, 0.0));
        assert_eq!(Complex(0.0, 2.0) - Complex(0.0, 1.0), Complex(0.0, 1.0));
        assert_eq!(Complex(2.0, 2.0) - Complex(1.0, 1.0), Complex(1.0, 1.0));
    }

    #[test]
    fn should_mul_correctly() {
        assert_eq!(Complex(2.0, 0.0) * Complex(2.0, 0.0), Complex(4.0, 0.0));
        assert_eq!(Complex(0.0, 2.0) * Complex(0.0, 2.0), Complex(-4.0, 0.0));
        assert_eq!(Complex(2.0, 2.0) * Complex(2.0, 2.0), Complex(0.0, 8.0));
    }

    #[test]
    fn should_div_correctly() {
        assert_eq!(Complex(4.0, 0.0) / Complex(2.0, 0.0), Complex(2.0, 0.0));
        assert_eq!(Complex(0.0, 4.0) / Complex(0.0, 2.0), Complex(-2.0, 0.0));
        assert_eq!(Complex(4.0, 4.0) / Complex(2.0, 2.0), Complex(2.0, 0.0));
        assert_eq!(Complex(4.0, 4.0) / Complex(2.0, 0.0), Complex(2.0, 2.0));
        assert_eq!(Complex(4.0, 4.0) / Complex(0.0, 2.0), Complex(-2.0, 2.0));
    }
}
